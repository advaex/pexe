git repo: https://gitlab.com/advaex/pexe  
npm repo: https://www.npmjs.com/package/pexe

# Installation
`npm install pexe` or `yarn add pexe`

# About
This is library to parse Windows executable files.
