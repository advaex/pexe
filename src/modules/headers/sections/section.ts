import {DataBlock, DataType, DataSection} from '../../binary'

// Описывает блок в таблице секций
export default class Section extends DataSection {
  fields = {
    // Название секции
    Name: new DataBlock(DataType.Byte * 8),
    // Размер секции в виртуальной памяти
    VirtualSize: new DataBlock(DataType.DWord),
    // Адрес секции в виртуальной памяти RVA
    VirtualAddress: new DataBlock(DataType.DWord),
    // Размер  секции в файле
    SizeOfRawData: new DataBlock(DataType.DWord),
    // RAW смещение до начала секции
    // TODO: Также должен быть кратен FileAligment
    PointerToRawData: new DataBlock(DataType.DWord),
    //
    PointerToRelocations: new DataBlock(DataType.DWord),
    //
    PointerToLinenumbers: new DataBlock(DataType.DWord),
    //
    NumberOfRelocations: new DataBlock(DataType.Word),
    //
    NumberOfLinenumbers: new DataBlock(DataType.Word),
    // Атрибуты доступа к секции и правила для её загрузки в вирт. память.
    // TODO: Прогуглить их: https://docs.microsoft.com/ru-ru/windows/win32/api/winnt/ns-winnt-image_section_header?redirectedfrom=MSDN
    Characteristics: new DataBlock(DataType.DWord),
  }
}
