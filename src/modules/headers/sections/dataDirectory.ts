import {DataBlock, DataType, DataSection} from '../../binary'

// Описывает блок в таблице секций
export default class DataDirectory extends DataSection {
  fields = {
    VirtualAddress: new DataBlock(DataType.DWord),
    Size: new DataBlock(DataType.DWord),
  }
}
