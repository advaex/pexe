import {DataBlock, DataType, DataSection} from '../binary'

// Описывает File Header
export default class FileHeader extends DataSection {
  // Поля заголовка
  fields = {
    // Architecture type of the computer
    Machine: new DataBlock(DataType.Word),
    // Size of the section table
    NumberOfSections: new DataBlock(DataType.Word),
    // Date and time the image was created
    TimeDataStamp: new DataBlock(DataType.DWord),
    // Offset of the symbol table, or zero if no COFF symbol table exists
    PointerToSymbolTable: new DataBlock(DataType.DWord),
    // Number of symbols in the symbol table
    NumberOfSymbols: new DataBlock(DataType.DWord),
    // NtOptional32Header
    SizeOfOptionalHeader: new DataBlock(DataType.Word),
    // ExecutableImage
    Characteristics: new DataBlock(DataType.Word),
  }
}
