import {DataBlock, DataType, DataSection} from '../binary'
import DataDirectories from "./sections/dataDirectories"

// Описывает Optional Header
export default class OptionalHeader extends DataSection {
  // Поля заголовка
  fields = {
    // State of the image file
    Magic: new DataBlock(DataType.Word),
    // Major version of linker
    MajorLinkerVersion: new DataBlock(DataType.Byte),
    // Minor version of linker
    MinorLinkerVersion: new DataBlock(DataType.Byte),
    // Size of the code section
    SizeOfCode: new DataBlock(DataType.DWord),
    // Size of the initialized data section
    SizeOfInitializedData: new DataBlock(DataType.DWord),
    // Size of the uninitialized data section
    SizeOfUninitializedData: new DataBlock(DataType.DWord),
    // Pointer to the entry point function, relative to the image base address, or zero if no entry point is present
    AddressOfEntryPoint: new DataBlock(DataType.DWord),
    // Pointer to the beginning of the code section, relative to the image base
    BaseOfCode: new DataBlock(DataType.DWord),
    // Pointer to the beginning of the data section, relative to the image base
    BaseOfData: new DataBlock(DataType.DWord),
    // Preferred address of the first byte of the image when it is loaded in memory
    // TODO: В большистве случаев равен 0x00400000.
    ImageBase: new DataBlock(DataType.DWord),
    // Alignment of the section loaded in memory
    SectionAlignment: new DataBlock(DataType.DWord),
    // Alignment of the raw data of sections in the image file
    FileAlignment: new DataBlock(DataType.DWord),
    // Major version number of the required operating system
    MajorOperatingSystemVersion: new DataBlock(DataType.Word),
    // Minor version number of the required operating system
    MinorOperatingSystemVersion: new DataBlock(DataType.Word),
    //
    MajorImageVersion: new DataBlock(DataType.Word),
    //
    MinorImageVersion: new DataBlock(DataType.Word),
    //
    MajorSubsystemVersion: new DataBlock(DataType.Word),
    //
    MinorSubsystemVersion: new DataBlock(DataType.Word),
    // Reserved
    Win32VersionValue: new DataBlock(DataType.DWord),
    // Size of the image including all headers
    // TODO: Проверять этот пункт. Если не совпадает с размером файла - подозрительно
    SizeOfImage: new DataBlock(DataType.DWord),
    //
    SizeOfHeaders: new DataBlock(DataType.DWord),
    // Image file checksum
    CheckSum: new DataBlock(DataType.DWord),
    // Subsystem required to run this image
    Subsystem: new DataBlock(DataType.Word),
    // DLL characteristics of the image
    DllCharacteristics: new DataBlock(DataType.Word),
    // Number of bytes to reserve for the stack
    SizeOfStackReserve: new DataBlock(DataType.DWord),
    // Number of bytes to commit for the stack
    SizeOfStackCommit: new DataBlock(DataType.DWord),
    // Number of bytes to reserve for the local heap
    SizeOfHeapReserve: new DataBlock(DataType.DWord),
    // Number of bytes to commit for the local heap
    SizeOfHeapCommit: new DataBlock(DataType.DWord),
    // Obsolete
    LoaderFlags: new DataBlock(DataType.DWord),
    // Number of directory entries in the remainder of the optional header
    // TODO: Всегда равно 16
    NumberOfRvaAndSizes: new DataBlock(DataType.DWord),
  }

  // ВЛоженные секции
  to = {
    dataDirectory: new DataDirectories(),
  }
}
