import DataSection from "../binary/dataSection";
import DataBlock from "../binary/dataBlock";
import {DataType} from "../binary/dataType";
import Pexe from "../../pexe";

// Описывает таблицу секций
export default class ExportDirectory extends DataSection {
  // Поля структуры
  fields = {
    //
    Characteristics: new DataBlock(DataType.DWord),
    //
    TimeDateStamp: new DataBlock(DataType.DWord),
    //
    MajorVersion: new DataBlock(DataType.Word),
    //
    MinorVersion: new DataBlock(DataType.Word),
    //
    Name: new DataBlock(DataType.DWord),
    //
    Base: new DataBlock(DataType.DWord),
    //
    NumberOfFunctions: new DataBlock(DataType.DWord),
    //
    NumberOfNames: new DataBlock(DataType.DWord),
    //
    AddressOfFunctions: new DataBlock(DataType.DWord),
    //
    AddressOfNames: new DataBlock(DataType.DWord),
    //
    AddressOfNameOrdinals: new DataBlock(DataType.DWord),
  }

  parse(data: DataBlock, offset: number = 0): this {
    return super.parse(data, offset);
  }
}
