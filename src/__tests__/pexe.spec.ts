import fs from 'fs'

const assert = require('assert').strict
import Pexe from '../pexe'
import {DataBlock} from '@/binary'

// Тест на общий парсинг файлов

const files = fs.readdirSync(`${__dirname}/examples/`)
  .map(name => ({
    name,
    data: new Uint8Array(fs.readFileSync(`${__dirname}/examples/${name}`))
  }))

describe('Pexe file parsing test', () => {
  files.forEach(file => {
    it(`file "${file.name}" parsing`, () => {
      let block = new DataBlock().setBytes(file.data)
      let exe = new Pexe(block)
      assert.equal(exe.headers.isValid(), true)
    })
  })
})
